import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListArticlesRoutingModule } from './list-articles-routing.module';
import { ArticleComponent } from './components/article/article.component';
import { CartSizeComponent } from './components/cart-size/cart-size.component';
import { FormsModule } from '@angular/forms';
import { ListeArticlesComponent } from './components/liste-articles/liste-articles.component';



@NgModule({
  declarations: [
    ArticleComponent,CartSizeComponent, ListeArticlesComponent],
  imports: [
    CommonModule,
    ListArticlesRoutingModule,
    FormsModule

  ]
})

export class ListArticlesModule { }
