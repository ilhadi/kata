import { Component, Inject } from '@angular/core';
import { Article } from 'src/app/core/models/article';
import { ArticlesService } from '../../services/articles.service';
import { Observable } from 'rxjs';
import { getArticles, selectArticlesByCategory } from '../../states/articles/articles.selectors';
import { Store } from '@ngrx/store';
import { addToCart, loadArticles, loadArticlesSuccess } from '../../states/articles/articles.actions';
import { PanierService } from '../../../panier/services/panier.service';

import { Order } from 'src/app/core/models/Order';
import { TaxCalculationServiceToken } from 'src/app/tokens';
import { TaxCalculationService } from 'src/app/core/services/impl/taxcalculator-service';

@Component({
  selector: 'app-liste-articles',
  templateUrl: './liste-articles.component.html',
  styleUrls: ['./liste-articles.component.scss']
})

export class ListeArticlesComponent {

  articles$: Observable<Article[]>;
  categories: string[] = [];
  selectedCategory: string = '';

  constructor(
     private articleService: ArticlesService,
     private panierService: PanierService,
    @Inject(TaxCalculationServiceToken) private taxCalculator: TaxCalculationService,
    private store: Store,
  ) {
    this.articles$ = store.select(getArticles);
  }


  ngOnInit() {
    this.getArticles();
  }

  getArticles(): void {
    this.store.dispatch(loadArticles());
    if (!this.articleService.articlesLoaded) {
      this.articleService.getArticles().subscribe(
        (articles) => {
          this.store.dispatch(loadArticlesSuccess({ articles }));
          this.articleService.articlesLoaded = true;
          this.categories = this.extractCategories(articles);
        });
    }
  }

  addToPanier(article: Article): void {
    this.store.dispatch(addToCart({ item: article }));
    const tax = this.taxCalculator.calculateTaxes(article)
    const order: Order = {
      article: article,
      tax: tax,
      pricettc: article.price + tax,
    };
    this.panierService.addToCart(order);
  }

  extractCategories(articles: Article[]): string[] {
    const uniqueCategories = new Set<string>();
    articles.forEach((article) => {
      uniqueCategories.add(article.category);
    });
    return Array.from(uniqueCategories);
  }

  filterArticlesByCategory(category: string): void {
    if (category) {
      this.articles$ = this.store.select(selectArticlesByCategory(category));
    } else {
      this.articles$ = this.store.select(getArticles);
    }
  }

}
