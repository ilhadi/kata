import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ArticlesService } from '../../services/articles.service';
import { Store, StoreModule } from '@ngrx/store';
import { PanierService } from '../../../panier/services/panier.service';
import { ListeArticlesComponent } from './liste-articles.component';
import { TaxCalculationService } from 'src/app/core/services/impl/taxcalculator-service';
import { TaxCalculationServiceToken } from 'src/app/tokens';
import { loadArticles } from '../../states/articles/articles.actions';
import { of } from 'rxjs';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';


describe('ListeArticlesComponent', () => {

  let component: ListeArticlesComponent;
  let articlesService: ArticlesService;
  let store: Store;
  let taxService: TaxCalculationService;
  let panierService: PanierService;

  const mockArticles = [
    { category: 'Books', isImported: true, price: 100, quantity: 1, id: 0, productName: 'Product A'},
    { category: 'Food', isImported: true, price: 100, quantity: 1, id: 1, productName: 'Product B'}
  ];

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [StoreModule.forRoot({}),HttpClientTestingModule],
      providers: [
        ListeArticlesComponent,
        ArticlesService, // Include the actual service
        Store, // Include the actual service
        PanierService, // Include the actual service
        { provide: TaxCalculationServiceToken, useValue: taxService },
      ],
    });
    component = TestBed.inject(ListeArticlesComponent);
    articlesService = TestBed.inject(ArticlesService);
    panierService = TestBed.inject(PanierService);
    store = TestBed.inject(Store);
  });

  it('should create the component', () => {
    expect(component).toBeTruthy();
  });

  it('should dispatch loadArticles', () => {
    const mockArticles = [{ category: 'Books', isImported: true, price: 100, quantity: 1, id: 0, productName: 'Product A'}];
    spyOn(articlesService, 'getArticles').and.returnValue(of(mockArticles));
    spyOn(store, 'dispatch');

    component.getArticles();

    expect(store.dispatch).toHaveBeenCalledWith(loadArticles());
  });

  it('should extract categories from articles', () => {
 
    const uniqueCategories = component.extractCategories(mockArticles);

    expect(uniqueCategories).toEqual(['Books', 'Food']);
  });
  
});