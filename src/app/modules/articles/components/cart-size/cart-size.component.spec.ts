import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CartSizeComponent } from './cart-size.component';
import { Store, StoreModule } from '@ngrx/store'; // Import the necessary NgRx modules
import { PanierService } from 'src/app/modules/panier/services/panier.service';

describe('CartSizeComponent', () => {
  let component: CartSizeComponent;
  let panierService: jasmine.SpyObj<PanierService>;

  beforeEach(() => {
    // Create a spy object for PanierService
    panierService = jasmine.createSpyObj('PanierService', ['getCartCountItems']);

    TestBed.configureTestingModule({
      declarations: [CartSizeComponent],
      imports: [
        StoreModule.forRoot({}) // Configure the StoreModule with an initial state
      ],
      providers: [
        { provide: PanierService, useValue: panierService }
      ]
    });

    component = TestBed.createComponent(CartSizeComponent).componentInstance;
  });

  it('should create the component', () => {
    expect(component).toBeTruthy();
  });

});