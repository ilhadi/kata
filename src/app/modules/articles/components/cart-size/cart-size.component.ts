import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { PanierService } from 'src/app/modules/panier/services/panier.service';

@Component({
  selector: 'app-cart-size',
  templateUrl: './cart-size.component.html',
  styleUrls: ['./cart-size.component.scss']
})
export class CartSizeComponent {

  cartItemCount$: Observable<number> | undefined;

  constructor(private panierService: PanierService) {
    this.getCartCountItems();
  }

  getCartCountItems(): void {
    this.cartItemCount$ = this.panierService.getCartCountItems();
  }

}
