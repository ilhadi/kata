import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ArticleComponent } from './article.component';


describe('ArticleComponent', () => {
  let component: ArticleComponent;
  let fixture: ComponentFixture<ArticleComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ArticleComponent],
    });
    
    fixture = TestBed.createComponent(ArticleComponent);
    component = fixture.componentInstance;
  });

  it('should create the component', () => {
    expect(component).toBeTruthy();
  });

  it('should return true for isAvailable when article quantity is greater than 0', () => {
    component.article = {
      id: 1,
      productName: 'Example Product',
      price: 10.00,
      quantity: 3, // Quantity greater than 0
      isImported: false,
      category: 'Books',
    };

    const result = component.isAvailable();
    expect(result).toBe(true);
  });

  it('should return false for isAvailable when article quantity is 0', () => {
    component.article = {
      id: 1,
      productName: 'Example Product',
      price: 10.00,
      quantity: 0, // Quantity is 0
      isImported: false,
      category: 'Books',
    };

    const result = component.isAvailable();
    expect(result).toBe(false);
  });
});
