import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Article } from 'src/app/core/models/article';

@Component({
  selector: 'article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss']
})
export class ArticleComponent {
  @Input() article: Article | undefined;
  @Output() ajouter: EventEmitter<Article> = new EventEmitter();

  addToPanier(article: Article): void {
    this.ajouter.emit(article);
  }

  isAvailable(): boolean {
    return this.article!.quantity > 0;
  }

}
