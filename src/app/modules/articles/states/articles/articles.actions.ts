import { createAction, props } from '@ngrx/store';
import { Article } from 'src/app/core/models/article';

export const loadArticles = createAction('[Article] Load Articles');
export const loadArticlesSuccess = createAction(
    '[Article] Load Articles Success',
    props<{ articles: Article[] }>()
);
export const loadArticlesFailure = createAction(
    '[Article] Load Articles Failure',
    props<{ error: any }>()
);
export const addToCart = createAction(
    '[Article] Add To Cart',
    props<{ item: Article }>()
);

export const removeFromCart = createAction(
    '[Article] Remove from Cart',
    props<{ item: Article }>()
);