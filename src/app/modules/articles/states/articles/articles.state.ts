import { Article } from "src/app/core/models/article";


export interface ArticleState {
    articles: Article[]; // This array will store your articles
  }
  
  export const initialArticleState: ArticleState = {
    articles: [],
  };