import { createFeatureSelector, createSelector } from '@ngrx/store';
import { ArticleState } from './articles.state';

const getArticleFeatureState = createFeatureSelector<ArticleState>('articles');

export const getArticles = createSelector(
    getArticleFeatureState,
    (state) => state.articles
);

export const selectArticlesByCategory = (category: string) =>createSelector(
    getArticleFeatureState, (state) =>
        state.articles.filter((article) => article.category === category)
    );