import * as fromActions from './articles.actions'; // Replace 'my-actions' with your actual pathconst mockArticle: Article = {
import { articleReducer } from './articles.reducer';
import { getArticles, selectArticlesByCategory } from './articles.selectors';

describe('My Actions', () => {
    it('should create the loadArticles action', () => {
        const action = fromActions.loadArticles();
        expect(action.type).toEqual('[Article] Load Articles');
    });

    it('should create the loadArticlesSuccess action', () => {
        const mockArticles = [{
            "id": 17,
            "productName": "Muffin Batt - Carrot Spice",
            "price": 3.84,
            "quantity": 5,
            "isImported": true,
            "category": "Food"
        },
        {
            "id": 6,
            "productName": "Goldschalger",
            "price": 9.71,
            "quantity": 5,
            "isImported": true,
            "category": "Food"
        },
        ];
        const action = fromActions.loadArticlesSuccess({ articles: mockArticles });
        expect(action.type).toEqual('[Article] Load Articles Success');
        expect(action.articles).toEqual(mockArticles);
    });

    it('should create the loadArticlesFailure action', () => {
        const error = 'Sample error message';
        const action = fromActions.loadArticlesFailure({ error });
        expect(action.type).toEqual('[Article] Load Articles Failure');
        expect(action.error).toEqual(error);
    });

    it('should create the addToCart action', () => {
        const mockArticle = {
            "id": 6,
            "productName": "Goldschalger",
            "price": 9.71,
            "quantity": 5,
            "isImported": true,
            "category": "Food"
        };
        const action = fromActions.addToCart({ item: mockArticle });
        expect(action.type).toEqual('[Article] Add To Cart');
        expect(action.item).toEqual(mockArticle);
    });

    it('should create the removeFromCart action', () => {
        const mockArticle = {
            "id": 6,
            "productName": "Goldschalger",
            "price": 9.71,
            "quantity": 5,
            "isImported": true,
            "category": "Food"
        };
        const action = fromActions.removeFromCart({ item: mockArticle });
        expect(action.type).toEqual('[Article] Remove from Cart');
        expect(action.item).toEqual(mockArticle);
    });
});

describe('Article Reducer', () => {
    it('should return the initial state', () => {
        const initialState = articleReducer(undefined, { type: 'unknown' });
        expect(initialState).toEqual({
            articles: []
        });
    });

    it('should handle loadArticles action', () => {
        const initialState = {
            articles: []
        };
        const nextState = articleReducer(initialState, fromActions.loadArticles());
        expect(nextState).toEqual({
            articles: []
        });
    });

    it('should handle loadArticlesSuccess action', () => {
        const initialState = {
            articles: []
        };
        const articles = [{
            "id": 17,
            "productName": "Muffin Batt - Carrot Spice",
            "price": 3.84,
            "quantity": 5,
            "isImported": true,
            "category": "Food"
        },
        {
            "id": 6,
            "productName": "Goldschalger",
            "price": 9.71,
            "quantity": 5,
            "isImported": true,
            "category": "Food"
        }];
        const nextState = articleReducer(initialState, fromActions.loadArticlesSuccess({ articles }));
        expect(nextState).toEqual({
            articles
        });
    });

    it('should handle loadArticlesFailure action', () => {
        const initialState = {
            articles: []
        };
        const error = 'Sample error message';
        const nextState = articleReducer(initialState, fromActions.loadArticlesFailure({ error }));
        expect(nextState).toEqual({
            articles: []
        });
    });

    it('should handle addToCart action : Quantity reduced by 1', () => {
        const initialState = {
            articles: [
                { id: 1, quantity: 5, productName: "PRoduct 1", price: 3.84, isImported: true, category: "Food" },
                { id: 2, quantity: 3, productName: "PRoduct 2", price: 3.84, isImported: true, category: "Food" },
            ],
        };
        const item = { id: 2, quantity: 3, productName: "PRoduct 2", price: 3.84, isImported: true, category: "Food" };

        const nextState = articleReducer(initialState, fromActions.addToCart({ item }));
        expect(nextState.articles).toEqual([
            { id: 1, quantity: 5, productName: "PRoduct 1", price: 3.84, isImported: true, category: "Food" },
            { id: 2, quantity: 2, productName: "PRoduct 2", price: 3.84, isImported: true, category: "Food" },
        ]);
    });

    it('should handle removeFromCart action :  Quantity increased by 1', () => {
        const initialState = {
            articles: [
                { id: 1, quantity: 5, productName: "PRoduct 1", price: 3.84, isImported: true, category: "Food" },
                { id: 2, quantity: 3, productName: "PRoduct 2", price: 3.84, isImported: true, category: "Food" },
            ]
        };
        const item = { id: 2, quantity: 3, productName: "PRoduct 2", price: 3.84, isImported: true, category: "Food" };
        const nextState = articleReducer(initialState, fromActions.removeFromCart({ item }));
        expect(nextState.articles).toEqual([
            { id: 1, quantity: 5, productName: "PRoduct 1", price: 3.84, isImported: true, category: "Food" },
            { id: 2, quantity: 4, productName: "PRoduct 2", price: 3.84, isImported: true, category: "Food" },
        ]);
    });
});

describe('Article Selectors', () => {
    it('should select articles from the state', () => {
        const mockState = {
            articles: [
                { id: 1, quantity: 5, productName: "PRoduct 1", price: 3.84, isImported: true, category: "Food" },
                { id: 2, quantity: 3, productName: "PRoduct 2", price: 3.84, isImported: true, category: "Food" },
                { id: 3, quantity: 5, productName: "PRoduct 3", price: 3.84, isImported: true, category: "Electric" },
                { id: 4, quantity: 3, productName: "PRoduct 4", price: 3.84, isImported: true, category: "Parfum" },
            ],
        };

        const selectedArticles = getArticles.projector(mockState);

        expect(selectedArticles).toEqual(mockState.articles);
    });

    it('should select articles by category', () => {
        const mockState = {
            articles: [
                { id: 1, quantity: 5, productName: "PRoduct 1", price: 3.84, isImported: true, category: "Food" },
                { id: 2, quantity: 3, productName: "PRoduct 2", price: 3.84, isImported: true, category: "Food" },
                { id: 3, quantity: 5, productName: "PRoduct 3", price: 3.84, isImported: true, category: "Electric" },
                { id: 4, quantity: 3, productName: "PRoduct 4", price: 3.84, isImported: true, category: "Parfum" }
            ],
        };
        const categoryToSelect = 'Electric';

        const selectedArticles = selectArticlesByCategory(categoryToSelect).projector(mockState);

        const expectedArticles = mockState.articles.filter((article) => article.category === categoryToSelect);

        expect(selectedArticles).toEqual(expectedArticles);
    });
});