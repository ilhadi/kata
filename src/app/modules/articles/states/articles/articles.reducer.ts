import { createReducer, on } from '@ngrx/store';
import { initialArticleState } from './articles.state';
import * as ArticleActions from './articles.actions';

export const articleReducer = createReducer(
    initialArticleState,

    on(ArticleActions.loadArticles, (state) => {
      return { ...state };
    }),

    on(ArticleActions.loadArticlesSuccess, (state, { articles }) => {
      return { ...state, articles};
    }),

    on(ArticleActions.loadArticlesFailure, (state, { error }) => {
      return { ...state};
    }),

    on(ArticleActions.addToCart, (state, { item }) => {
        const updatedArticles = state.articles.map((article) => {
          if (article.id === item.id) {
            return { ...article, quantity: article.quantity - 1 };
          }
          return article;
        });
    
        return { ...state, articles: updatedArticles };
      }),

      
  on(ArticleActions.removeFromCart, (state, { item }) => {
    const updatedArticles = state.articles.map((article) => {
      if (article.id === item.id) {
        return { ...article, quantity: article.quantity + 1 };
      }
      return article;
    });

    return { ...state, articles: updatedArticles };
  }),

  
  );