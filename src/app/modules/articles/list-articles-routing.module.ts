import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListeArticlesComponent } from './components/liste-articles/liste-articles.component';

const routes: Routes = [{ path: '', redirectTo: '/articles', pathMatch: 'full' },
{ path: 'articles', component: ListeArticlesComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListArticlesRoutingModule { }
