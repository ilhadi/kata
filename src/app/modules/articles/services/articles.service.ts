import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Article } from 'src/app/core/models/article';
import { removeFromCart } from '../states/articles/articles.actions';

@Injectable({
  providedIn: 'root'
})
export class ArticlesService {

  articlesLoaded = false;
  articlesUrl = 'assets/articles.json'; // Articles JSON
  
  constructor(private http: HttpClient,private store: Store) {}

  getArticles(): Observable<Article[]> {
    return this.http.get<Article[]>(this.articlesUrl);
  }

  updateArticleQuantity(article: Article):void{
    this.store.dispatch(removeFromCart({ item: article }));
  }
}