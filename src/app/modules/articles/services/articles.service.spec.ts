import { TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ArticlesService } from './articles.service';
import { Store } from '@ngrx/store';
import { Article } from 'src/app/core/models/article';
import { removeFromCart } from '../states/articles/articles.actions';


describe('ArticlesService', () => {
  let service: ArticlesService;
  let httpMock: HttpTestingController;
  let store: Store;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        ArticlesService,
        { provide: Store, useValue: jasmine.createSpyObj('Store', ['dispatch']) },
      ],
    });

    service = TestBed.inject(ArticlesService);
    httpMock = TestBed.inject(HttpTestingController);
    store = TestBed.inject(Store);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get articles from the API via HTTP', () => {
    const mockArticles: Article[] = [
      { id: 1, productName: 'Product 1', price: 10.00, quantity: 10, isImported: false, category: 'Category 1' },
    ];

    service.getArticles().subscribe((articles) => {
      expect(articles).toEqual(mockArticles);
    });

    const req = httpMock.expectOne(service.articlesUrl);
    expect(req.request.method).toBe('GET');
    req.flush(mockArticles);
  });

  it('should dispatch removeFromCart action when updateArticleQuantity is called', () => {
    const article: Article = { id: 1, productName: 'Product 1', price: 10.00, quantity: 10, isImported: false, category: 'Category 1' };

    service.updateArticleQuantity(article);

    expect(store.dispatch).toHaveBeenCalledWith(removeFromCart({ item: article }));
  });

  afterEach(() => {
    httpMock.verify();
  });
});
