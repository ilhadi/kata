import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListeItemsComponent } from './components/liste-items/liste-items.component';

const routes: Routes = [{ path: 'panier', component: ListeItemsComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PanierRoutingModule { }
