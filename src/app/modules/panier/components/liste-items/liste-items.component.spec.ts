import { TestBed } from '@angular/core/testing';

import { TaxCalculationService } from 'src/app/core/services/impl/taxcalculator-service';
import { ListeItemsComponent } from './liste-items.component';
import { PanierService } from '../../services/panier.service';
import { ArticlesService } from 'src/app/modules/articles/services/articles.service';
import { of } from 'rxjs';
import { Order } from 'src/app/core/models/Order';
import { TaxCalculationServiceToken } from 'src/app/tokens';
import { Store, StoreModule } from '@ngrx/store';
import { HttpClientTestingModule } from '@angular/common/http/testing';

const mockOrders: Order[] = [
  { article: { category: 'Books', isImported: true, price: 100, quantity: 1, id: 0, productName: 'Product A' }, tax: 0.00, pricettc: 0.00 },
  { article: { category: 'Food', isImported: false, price: 50, quantity: 1, id: 0, productName: 'Product A' }, tax: 0.00, pricettc: 0.00 },
];
describe('ListeItemsComponent', () => {
    let component: ListeItemsComponent;
    let articlesService: ArticlesService;
    let panierService: PanierService;
    let taxService: TaxCalculationService;
  
    beforeEach(() => {
      TestBed.configureTestingModule({
        
      imports: [StoreModule.forRoot({}),HttpClientTestingModule],
        declarations: [ListeItemsComponent],
        providers: [
          ArticlesService,
          PanierService,
          { provide: TaxCalculationServiceToken, useClass: TaxCalculationService },
        ],
      });
  
      component = TestBed.createComponent(ListeItemsComponent).componentInstance;
      articlesService = TestBed.inject(ArticlesService);
      panierService = TestBed.inject(PanierService);
     // taxService = TestBed.inject(TaxCalculationService);
    });
  
    it('should create', () => {
      expect(component).toBeTruthy();
    });

    it('should get items from panierService', () => {
      spyOn(panierService, 'getCartItems').and.returnValue(of(mockOrders));
  
      component.getItems();
  
      expect(panierService.getCartItems).toHaveBeenCalled();
      expect(component.items).toEqual(mockOrders);
    });

    it('should remove an item from the cart and update article quantity', () => {
      const mockOrder = {
        article: { id: 1, productName: 'Product 1', price: 10.00, quantity: 10, isImported: false, category: 'Category 1' },
        tax: 1.5,
        pricettc: 11.5,
      };
      const spyRemoveFromCart = spyOn(panierService, 'removeFromCart');
      const spyUpdateArticleQuantity = spyOn(articlesService, 'updateArticleQuantity');
  
      component.removeFromCart(mockOrder);
  
      expect(spyRemoveFromCart).toHaveBeenCalledWith(mockOrder);
      expect(spyUpdateArticleQuantity).toHaveBeenCalledWith(mockOrder.article);
    });

});