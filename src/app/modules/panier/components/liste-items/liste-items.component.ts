import { Component, Inject } from '@angular/core';
import { ArticlesService } from 'src/app/modules/articles/services/articles.service';
import { TaxCalculationServiceToken } from 'src/app/tokens';
import { PanierService } from '../../services/panier.service';
import { TaxCalculationService } from 'src/app/core/services/impl/taxcalculator-service';
import { Order } from 'src/app/core/models/Order';

@Component({
  selector: 'app-liste-items',
  templateUrl: './liste-items.component.html',
  styleUrls: ['./liste-items.component.scss']
})
export class ListeItemsComponent {

  items: Order[] = [];

  constructor(
    private articleService: ArticlesService,
    private panierService: PanierService,
    @Inject(TaxCalculationServiceToken) private taxCalculator: TaxCalculationService,
  ) { }


  ngOnInit() {
    this.getItems();
  }

  getItems() : void{
    this.panierService.getCartItems().subscribe((orders) => {
      this.items = orders;
    });
  }

  removeFromCart(order: Order): void {
    this.panierService.removeFromCart(order);
    this.articleService.updateArticleQuantity(order.article);
  }

  calculeTotalTaxes(): number {
    return this.taxCalculator.calculateTotalTaxes(this.items);
  }

  calculeTotalTTC() : number{
    return this.taxCalculator.calculateTotalTTC(this.items);
  }
}
