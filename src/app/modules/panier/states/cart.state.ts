import { Order } from "src/app/core/models/Order";

export interface CartState {
  items: Order[];
}

export const initialCartState: CartState = {
  items: [],
};