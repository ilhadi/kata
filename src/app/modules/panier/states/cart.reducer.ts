
import { createReducer, on } from '@ngrx/store';
import { initialCartState } from './cart.state';
import * as CartActions from './cart.actions';
import { Article } from 'src/app/core/models/article';
import { Order } from 'src/app/core/models/Order';

export const cartReducer = createReducer(
  initialCartState,
 
  on(CartActions.addToCart, (state, { order }) => {
    const existingItem = state.items.find((i) => i.article.id === order.article.id);

    if (existingItem) {
      const updatedItems = state.items.map((i) => {
        if (i.article.id === order.article.id) {
          const updatedArticle: Article = {
            ...i.article,
            quantity: i.article.quantity + 1,
          };
          return { ...i, article: updatedArticle};
        }
        return i;
      });

      return { ...state, items: updatedItems };
    } else {
      const newOrder: Order = { ...order, article: { ...order.article, quantity: 1 } };
      return { ...state, items: [...state.items, newOrder] };
    }
  }),

  on(CartActions.removeFromCart, (state, { order }) => {
    const existingItem = state.items.find((i) => i.article.id === order.article.id);

    if (existingItem && existingItem.article.quantity > 1) {
      const updatedItems = state.items.map((i) => {
        if (i.article.id === order.article.id) {
          const updatedArticle: Article = {
            ...i.article,
            quantity: i.article.quantity - 1,
          };
          return { ...i, article: updatedArticle};
        }
        return i;
      });
      return { ...state, items: updatedItems };
    } else {
      return { ...state, items: state.items.filter((i) => i.article.id !== order.article.id) };
    }
  })
  
);