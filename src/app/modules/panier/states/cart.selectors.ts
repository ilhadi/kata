
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { CartState } from './cart.state';

const getCartFeatureState = createFeatureSelector<CartState>('cart');

export const getCartItems = createSelector(
  getCartFeatureState,
  (state) => state.items
);

export const getCartItemCount = createSelector(
    getCartItems,
    (items) => items.reduce((count, item) => count + item.article.quantity, 0)
  );
