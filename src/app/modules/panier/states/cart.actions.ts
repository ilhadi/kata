import { createAction, props } from '@ngrx/store';
import { Order } from 'src/app/core/models/Order';

export const addToCart = createAction('[Cart] Add to Cart', props<{ order: Order }>());
export const removeFromCart = createAction('[Cart] Remove from Cart', props<{ order: Order }>());