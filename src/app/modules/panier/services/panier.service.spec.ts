import { TestBed, inject } from '@angular/core/testing';
import { Store, StoreModule } from '@ngrx/store';
import { PanierService } from '../services/panier.service';
import * as CartActions from '../states/cart.actions';
import { getCartItems, getCartItemCount } from '../states/cart.selectors';



describe('PanierService', () => {
  let service: PanierService;
  let store: Store;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [StoreModule.forRoot({})], // Set up a minimal store for testing
      providers: [PanierService],
    });

    service = TestBed.inject(PanierService);
    store = TestBed.inject(Store);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should dispatch addToCart action when addToCart is called', () => {
    const order = {
      article: { id: 1, productName: 'Product 1', price: 10.00, quantity: 10, isImported: false, category: 'Category 1' },
      tax: 1.5,
      pricettc: 11.5,
    };

    const addToCartAction = CartActions.addToCart({ order });

    spyOn(store, 'dispatch');
    service.addToCart(order);

    expect(store.dispatch).toHaveBeenCalledWith(addToCartAction);
  });

  it('should dispatch removeFromCart action when removeFromCart is called', () => {
    const order = {
      article: { id: 1, productName: 'Product 1', price: 10.00, quantity: 10, isImported: false, category: 'Category 1' },
      tax: 1.5,
      pricettc: 11.5,
    };

    const removeFromCartAction = CartActions.removeFromCart({ order });

    spyOn(store, 'dispatch');
    service.removeFromCart(order);

    expect(store.dispatch).toHaveBeenCalledWith(removeFromCartAction);
  });


});
