import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Order } from 'src/app/core/models/Order';
import { getCartItemCount, getCartItems } from '../states/cart.selectors';
import * as CartActions from '../states/cart.actions'
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class PanierService {

  constructor(private store: Store) { }
  
  addToCart(order: Order): void {
    this.store.dispatch(CartActions.addToCart({ order }));
  }

  removeFromCart(order: Order): void {
    this.store.dispatch(CartActions.removeFromCart({ order }));
  }

  getCartItems() : Observable<Order[]> {
    return  this.store.select(getCartItems);
  }

  getCartCountItems() : Observable<number>{
    return this.store.select(getCartItemCount);
  }

}
