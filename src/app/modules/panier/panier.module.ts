import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PanierRoutingModule } from './panier-routing.module';
import { ListeItemsComponent } from './components/liste-items/liste-items.component';


@NgModule({
  declarations: [ListeItemsComponent],
  imports: [
    CommonModule,
    PanierRoutingModule,
  ]
})
export class PanierModule { }
