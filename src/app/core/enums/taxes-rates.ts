export enum TaxeRate {
    TAX_RATE_10_PERCENT = 0.1,
    TAX_RATE_0_PERCENT = 0.0,
    TAX_RATE_20_PERCENT = 0.2,
    TAX_RATE_5_PERCENT = 0.05,
};

