
import { TestBed } from '@angular/core/testing';
import { Order } from '../models/Order';
import { Article } from '../models/article';
import { Categories } from '../enums/categories';
import { TaxCalculationService } from './impl/taxcalculator-service';


describe('TaxCalculationServiceImpl', () => {
    let service: TaxCalculationService;

    const articleA: Article = { category: 'Books', isImported: true, price: 100, quantity: 1, id: 0, productName: 'Product A'};
    const articleB: Article = { category: 'Food', isImported: false, price: 50, quantity: 1, id: 0, productName: 'Product B' };
    const food: Article = { id: 2, productName: 'Chocolate',price: 3.00,quantity: 1,isImported: false,category: Categories.FOOD, };
    const orders: Order[] = [
        { article: { category: 'Books', isImported: true, price: 100, quantity: 1, id: 0, productName: 'Product A' }, tax: 0.00, pricettc: 0.00 },
        { article: { category: 'Food', isImported: false, price: 50, quantity: 1, id: 0, productName: 'Product A' }, tax: 0.00, pricettc: 0.00 },
    ];
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [TaxCalculationService],
        });

        service = TestBed.inject(TaxCalculationService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    describe('calculateTaxes', () => {
        it('should calculate tax for an imported article in the "Books" category', () => {
            const tax = service.calculateTaxes(articleA);
            expect(tax).toEqual(15.00); // Assuming 10% tax for Books and 5% import tax
        });

        it('should calculate tax for an unimported article in the "Food" category', () => {
            const tax = service.calculateTaxes(articleB);
            expect(tax).toEqual(0.0); // 0% tax for Food
        });

        it('should calculate taxes for non-imported food correctly', () => {
  
            const taxAmount = service.calculateTaxes(food);
            expect(taxAmount).toEqual(0.00);
        });

    });

    describe('calculateTotalTaxes', () => {
        it('should calculate total taxes for a list of orders correctly', () => {
            orders.forEach((order) => {
                order.tax = service.calculateTaxes(order.article);
                order.pricettc = order.article.price + order.tax;
            });

            const totalTaxes = service.calculateTotalTaxes(orders);

            expect(orders[0].tax).toEqual(15.00);
            expect(orders[1].tax).toEqual(0.00);


            expect(totalTaxes).toEqual(15.00);
        });

    });

    describe('calculateTotalTTC', () => {
        it('should calculate total TTC for a list of orders correctly', () => {
            const orders: Order[] = [
                {
                    article: { category: 'Books', isImported: true, price: 100, quantity: 1, id: 0, productName: 'Product A' }, tax: 0.00, pricettc: 0.00,
                },
                {
                    article: { category: 'Food', isImported: false, price: 50, quantity: 1, id: 0, productName: 'Product A' },
                    tax: 0.00,
                    pricettc: 0.00,
                },
            ];

            orders.forEach((order) => {
                order.tax = service.calculateTaxes(order.article);
                order.pricettc = order.article.price + order.tax;
            });

            const totalTTC = service.calculateTotalTTC(orders);
            expect(orders[0].pricettc).toEqual(115.00);
            expect(orders[1].pricettc).toEqual(50.00);
            expect(totalTTC).toEqual(165.00);
        });
    });
});
