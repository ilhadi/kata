import { Order } from "../models/Order";
import { Article } from "../models/article";

export interface TaxCalculator {
    calculateTaxes(article: Article): number;
    calculateTotalTaxes(orders: Order[]): number;
    calculateTotalTTC(orders: Order[]): number;
}

