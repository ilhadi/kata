import { Categories } from "../../enums/categories";
import { TaxeRate } from "../../enums/taxes-rates";
import { Order } from "../../models/Order";
import { Article } from "../../models/article";
import { TaxCalculator } from "../tax-calculator";


export class TaxCalculationService implements TaxCalculator {

  public calculateTaxes(article: Article): number {

    let taxRate: number = this.getTaxRaTeByCategorie(article);

    let taxAmount = article.price * taxRate;

    taxAmount = Math.ceil(taxAmount / 0.05) * 0.05;

    return parseFloat(taxAmount.toFixed(2));
  }

  public calculateTotalTaxes(orders: Order[]): number {
    return orders.reduce((totalTaxes, order) => {
      return totalTaxes + order.tax * order.article.quantity;
    }, 0);
  }

  public calculateTotalTTC(orders: Order[]): number {
    return orders.reduce((totalTTC, order) => {
      const itemTaxes = this.calculateTaxes(order.article);
      const itemTTC = order.article.price + itemTaxes;
      return totalTTC + itemTTC * order.article.quantity;
    }, 0);
  }

  public getTaxRaTeByCategorie(article: Article): number {

    const categoryTaxRates: Record<Categories, number> = {
      [Categories.BOOKS]: TaxeRate.TAX_RATE_10_PERCENT,
      [Categories.FOOD]: TaxeRate.TAX_RATE_0_PERCENT,
      [Categories.MEDICINE]: TaxeRate.TAX_RATE_0_PERCENT
    };

    const baseTaxRate = categoryTaxRates[article.category as Categories] ?? TaxeRate.TAX_RATE_20_PERCENT;

    const importTaxRate = article.isImported ? TaxeRate.TAX_RATE_5_PERCENT : 0;

    return baseTaxRate + importTaxRate;
  }
}