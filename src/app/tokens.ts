import { InjectionToken } from "@angular/core";
import { TaxCalculationService } from "./core/services/impl/taxcalculator-service";

export const TaxCalculationServiceToken = new InjectionToken<TaxCalculationService>('TaxCalculationService');
