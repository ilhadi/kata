import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from "@angular/common/http";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PanierModule } from './modules/panier/panier.module';
import { StoreDevtoolsModule } from "@ngrx/store-devtools";
import { MetaReducer, StoreModule } from '@ngrx/store';
import { localStorageSync } from 'ngrx-store-localstorage';
import { cartReducer } from './modules/panier/states/cart.reducer';
import { ListArticlesModule } from './modules/articles/list-articles.module';
import { articleReducer } from './modules/articles/states/articles/articles.reducer';
import { TaxCalculationServiceToken } from './tokens';
import { TaxCalculationService } from './core/services/impl/taxcalculator-service';
import { PanierService } from './modules/panier/services/panier.service';
import { ArticlesService } from './modules/articles/services/articles.service';

const localStorageSyncReducerForArticles = localStorageSync({ keys: ['articles'], rehydrate: true });
const localStorageSyncReducer = localStorageSync({ keys: ['cart'], rehydrate: true });

const metaReducers: Array<MetaReducer<any, any>> = [
  localStorageSyncReducerForArticles,
  localStorageSyncReducer];

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ListArticlesModule,
    PanierModule,
    StoreModule.forRoot({ cart: cartReducer, articles: articleReducer }, { metaReducers }),
    StoreDevtoolsModule.instrument()
  ],
  providers: [
    { provide: TaxCalculationServiceToken, useClass: TaxCalculationService }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
